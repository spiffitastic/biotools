import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.regex.*;

public class RestrictionEnzyme extends JFrame{
	public RestrictionEnzyme(){
		super("Enzyme Cutter");
		Container cont = getContentPane();
		cont.setLayout(new BoxLayout(cont, BoxLayout.PAGE_AXIS));
		
		//INPUT LABEL
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel labelRawInput = new JLabel("Sequence Input:");
		p1.add(labelRawInput);
		cont.add(p1);
		
		//INPUT TEXTBOX
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextArea textRawInput = new JTextArea(null,5,50);
		textRawInput.setLineWrap(true);
		textRawInput.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane inputPane = new JScrollPane(textRawInput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p2.add(inputPane);
		cont.add(p2);

		//ZYME PICKER LABEL
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel labelZymePicker = new JLabel("Select your enzymes:");
		p3.add(labelZymePicker);
		cont.add(p3);
		
		//ZYME PICKER
		JPanel p4 = new JPanel(new GridLayout(2,3));
		
		JCheckBox enzymeECORI = new JCheckBox("EcoRI");
		JCheckBox enzymeBAMHI = new JCheckBox("BamHI");
		JCheckBox enzymePVUII = new JCheckBox("PvuII");
		JCheckBox enzymeSACI = new JCheckBox("SacI");
		JCheckBox enzymeCLAI = new JCheckBox("ClaI");
		JCheckBox enzymeXBAI = new JCheckBox("XbaI");
		
		p4.add(enzymeBAMHI);
		p4.add(enzymeCLAI);
		p4.add(enzymeECORI);
		p4.add(enzymePVUII);
		p4.add(enzymeSACI);
		p4.add(enzymeXBAI);
		
		cont.add(p4);
		
		//BUTTONS
		JPanel p5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JButton cutButton = new JButton("Cut");
		cutButton.setMnemonic('C');
		
		JButton resetButton = new JButton("Reset");
		resetButton.setMnemonic('R');
		
		p5.add(cutButton);
		p5.add(resetButton);
		cont.add(p5);
		
		//OUTPUT LABEL
		JPanel p6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel labelSummary = new JLabel("Summary:");
		p6.add(labelSummary);
		cont.add(p6);
		
		//OUTPUT SUMMARY
		JPanel p7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextArea textSummary = new JTextArea("",10,50);
		textSummary.setEditable(false);
		textSummary.setLineWrap(true);
		textSummary.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane summaryPane = new JScrollPane(textSummary, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p7.add(summaryPane);
		cont.add(p7);		
		
		setSize(585,550);
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		RestrictionEnzyme application = new RestrictionEnzyme();
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}