package fastaconverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.regex.*;

public class Main extends JFrame {
	// VARS
	private JButton justDoIt, reset;
	private JCheckBox spacingOut;
	private JComboBox dropMenu;
	private JLabel labelRawInput,labelRefinedOutput,labelPerLine,labelGroupByTen,labelCaseToggle,labelSummary;
	private JRadioButton upperCaseRadio, lowerCaseRadio;
	private JScrollPane inputPane, outputPane, summaryPane;
	private JTextArea textRawInput,textRefinedOutput,textSummary;
	private int linesize, seqLength;
	private boolean spaceEveryTen;
	private String radioChoice, rawSequence, refinedSequence;

	public Main() {
		super("Sequence Formatting Tool");
		Container cont = getContentPane();
		cont.setLayout(new BoxLayout(cont, BoxLayout.PAGE_AXIS));
		
		//INPUT LABEL
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		labelRawInput = new JLabel("Raw input:");
		p1.add(labelRawInput);
		cont.add(p1);
		
		//INPUT TEXTBOX
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		textRawInput = new JTextArea(null,5,60);
		textRawInput.setLineWrap(true);
		textRawInput.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane inputPane = new JScrollPane(textRawInput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p2.add(inputPane);
		cont.add(p2);
		
		//OUTPUT LABEL
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		labelRefinedOutput = new JLabel("Refined Output:");
		p3.add(labelRefinedOutput);
		cont.add(p3);
		
		//OUTPUT TEXTBOX (NONEDITABLE)
		JPanel p4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		textRefinedOutput = new JTextArea(null,5,60);
		textRefinedOutput.setEditable(false);
		textRefinedOutput.setLineWrap(true);
		textRefinedOutput.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane outputPane = new JScrollPane(textRefinedOutput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p4.add(outputPane);
		cont.add(p4);		
		
		//LABELS FOR DROPDOWNS,CHKBX,RADIOS
		JPanel p5 = new JPanel(new GridLayout(3,4));
		labelPerLine = new JLabel("Bases per line:");
		labelGroupByTen = new JLabel("Group in tens:");
		labelCaseToggle = new JLabel("Select case:");
		
		//DROP DOWN MENU
		String[] set = {"40","50","60","70"};
		dropMenu = new JComboBox<String>(set);
		dropMenu.setSelectedItem("60");
		linesize = dropMenu.getSelectedIndex();
		
		ActionListener listenLinesize = new ActionListener() {
			public void actionPerformed(ActionEvent linesizeEvent) {
				linesize = dropMenu.getSelectedIndex(); //0,1,2,3
			}
		};
		dropMenu.addActionListener(listenLinesize);
		
		//CHECKBOX (GROUP BY TENS)
		spacingOut = new JCheckBox("Yes");
		ActionListener listenSpacing = new ActionListener() {
			public void actionPerformed(ActionEvent spacingEvent) {
				spaceEveryTen = spacingOut.isSelected();
			}
		};
		spacingOut.addActionListener(listenSpacing);
				
		//RADIO BUTTONS (UPPER LOWER)
		JPanel p5a = new JPanel(new GridLayout(1,2));
		upperCaseRadio = new JRadioButton("Upper");
		lowerCaseRadio = new JRadioButton("Lower");
		
		ButtonGroup bGroup = new ButtonGroup();
        bGroup.add(upperCaseRadio);
        bGroup.add(lowerCaseRadio);
		
		lowerCaseRadio.setSelected(true);
		radioChoice = "lower";
		
		ActionListener listenRadioUpper = new ActionListener() {
			public void actionPerformed(ActionEvent radioEventUpper) {
				if (upperCaseRadio.isSelected()) {
					radioChoice = "upper";
				} 
			}
		};
		upperCaseRadio.addActionListener(listenRadioUpper);
	
		ActionListener listenRadioLower = new ActionListener() {
			public void actionPerformed(ActionEvent radioEventLower) {
				if (lowerCaseRadio.isSelected()) {
					radioChoice = "lower";
				} 
			}
		};
		lowerCaseRadio.addActionListener(listenRadioLower);
		
		//ADD THE LABELS AND COMPONENTS APPROPRIATE PANELS
		//AND PANELS TO CONTAINER
		p5a.add(upperCaseRadio);
		p5a.add(lowerCaseRadio);
		p5.add(labelPerLine);
		p5.add(dropMenu);
		p5.add(new JLabel(""));
		p5.add(new JLabel(""));
		
		p5.add(labelGroupByTen);
		p5.add(spacingOut);
		p5.add(new JLabel(""));
		p5.add(new JLabel(""));
		
		p5.add(labelCaseToggle);
		p5.add(p5a);
		cont.add(p5);
		
		//CONFIRM/RESET BUTTONS
		JPanel p7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel p7a = new JPanel(new GridLayout(1,4));
		
		justDoIt = new JButton("Format");
		justDoIt.setMnemonic('F');
		
		justDoIt.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent formatButtonAction) {
					
					//get the sequence length, remove all whitespace(for invalid check)
					rawSequence = textRawInput.getText().replaceAll("\\s+","");
					seqLength = rawSequence.length();
					
					//check if there are invalid bases
					Pattern invalidPattern = Pattern.compile("[^atcgATCG]");
					Matcher invalidMatch = invalidPattern.matcher(rawSequence);
					int countInvalid = 0;
					while (invalidMatch.find()) {
						countInvalid++;
					}
					
					if (countInvalid > 0) {
						JOptionPane.showMessageDialog(null,"Invalid characters found in sequence.");
						//show user a msgBOX if it finds anything other than a/t/c/g
					} else {
						//COUNT THE A/T/C/G
						Pattern aPattern = Pattern.compile("[aA]");
						Matcher aMatcher = aPattern.matcher(rawSequence);
						int aCount = 0;
						while (aMatcher.find()) {
							aCount++;
						}
						
						Pattern tPattern = Pattern.compile("[tT]");
						Matcher tMatcher = tPattern.matcher(rawSequence);
						int tCount = 0;
						while (tMatcher.find()) {
							tCount++;
						}
						
						Pattern cPattern = Pattern.compile("[cC]");
						Matcher cMatcher = cPattern.matcher(rawSequence);
						int cCount = 0;
						while (cMatcher.find()) {
							cCount++;
						}
						
						Pattern gPattern = Pattern.compile("[gG]");
						Matcher gMatcher = gPattern.matcher(rawSequence);
						int gCount = 0;
						while (gMatcher.find()) {
							gCount++;
						}
						//FORMAT ACCORDING TO USER CHOICE - BASES PER LINE
						if (linesize == 0){
							refinedSequence = rawSequence.replaceAll("(.{40})", "$1\n");
						} else if (linesize == 1){
							refinedSequence = rawSequence.replaceAll("(.{50})", "$1\n");
						} else if (linesize == 2){
							refinedSequence = rawSequence.replaceAll("(.{60})", "$1\n");
						} else if (linesize == 3){
							refinedSequence = rawSequence.replaceAll("(.{70})", "$1\n");
						}
						
						//FORMAT ACCORDING TO USER CHOICE - SPACE EVERY TEN
						if (spaceEveryTen == true){
							refinedSequence = refinedSequence.replaceAll("(.{10})","$1 ");
							refinedSequence = refinedSequence.replaceAll(" \n","\n");
						}
						
						//FORMAT ACCORDING TO USER CHOICE - CASE SELECTION
						if (radioChoice == "upper") {
							refinedSequence = refinedSequence.toUpperCase();
						} else if (radioChoice == "lower") {
							refinedSequence = refinedSequence.toLowerCase();
						}
						
						textRefinedOutput.setText(refinedSequence);
						
						//SUMMARY BOX
	
						// CALCULATE THE PERCENTS
						int percentA = (int)((double)aCount*100/seqLength);
						int percentT = (int)((double)tCount*100/seqLength);
						int percentC = (int)((double)cCount*100/seqLength);
						int percentG = (int)((double)gCount*100/seqLength);
						
						//WRITE EM TO THE SUMMARY BOX
						textSummary.setText("Percentage composition:");
						textSummary.append("\nA: " + percentA + "%");
						textSummary.append("\nT: " + percentT + "%");
						textSummary.append("\nC: " + percentC + "%");
						textSummary.append("\nG: " + percentG + "%");
						textSummary.append("\n");
						textSummary.append("\nSequence Length: " + seqLength);
					}
				}
			}
		);
		
		//RESET BUTTON
		reset = new JButton("Reset");
		reset.setMnemonic('R');
		
		reset.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent resetButtonAction) {
					int youSureAboutThat = JOptionPane.showConfirmDialog(null,"Are you sure?");
					if (youSureAboutThat == JOptionPane.YES_OPTION) {
						
						//reset textRawInput,textRefinedOutput,textSummary
						textRawInput.setText(null);
						textRefinedOutput.setText(null);
						textSummary.setText("");
						//reset dropmenu
						dropMenu.setSelectedItem("60");
						linesize = dropMenu.getSelectedIndex();
						//reset checkbox
						spacingOut.setSelected(false);
						spaceEveryTen = false;
						//reset radios
						lowerCaseRadio.setSelected(true);
						radioChoice = "lower";
					}
				}
			}
		);
		
		p7.add(justDoIt);
		p7.add(reset);
		cont.add(p7);
		
		//OUTPUT LABEL
		JPanel p8 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		labelSummary = new JLabel("Summary:");
		p8.add(labelSummary);
		cont.add(p8);
		
		//OUTPUT SUMMARY
		JPanel p9 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		textSummary = new JTextArea("",8,60);
		textSummary.setEditable(false);
		textSummary.setLineWrap(true);
		textSummary.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane summaryPane = new JScrollPane(textSummary, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p9.add(summaryPane);
		cont.add(p9);	

		// FINAL STUFF
		setSize(700,550);
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		Main application = new Main();
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}